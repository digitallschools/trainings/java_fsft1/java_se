
import java.util.Optional;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class OptionalDemo {
    public static void main(String[] args) {
        Optional<Integer> t = Optional.of(10);
        
        Optional<Integer> n = t.filter(x -> x==10);
        Optional<Integer> m = t.filter(x -> x==12);
        
        System.out.println(n);
        System.out.println(m);
    }
}
