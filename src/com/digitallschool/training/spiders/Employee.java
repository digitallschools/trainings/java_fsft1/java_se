/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Employee extends Object {

    private int employeeId;
    private String name;
    private String designation;

    @Override
    public String toString() {
        return "Employee[ID: " + employeeId + ", Name: " + name
                + ", Desg: " + designation + "]";
    }

    public Employee() {

    }

    public Employee(int eid, String n) {
        employeeId = eid;
        name = n;
    }

    public Employee(int eid, String n, String d) {
        employeeId = eid;
        name = n;
        designation = d;
    }

    public void assignWork() {
        System.out.println("Work is assigned");
    }

    public static void whoAreYou() {
        System.out.println("I am an Employee");
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

}
