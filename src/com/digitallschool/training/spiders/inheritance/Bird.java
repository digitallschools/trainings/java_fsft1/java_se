/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.inheritance;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Bird extends Animal {

    boolean fly;
    
    public Bird(boolean f){
        super();
        fly = f;
    }

    public void move(int distance) {
        super.move(distance);
        if (fly) {
            System.out.println("Bird is flying: " + distance + " distance");
        } else {
            System.out.println("Bird is moving: " + distance + " distance");
        }
    }
    
    public static void whoAmI(){
        System.out.println("I am Bird");
    }
}
