/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.inheritance;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public abstract class Account {

    private int accountNumber;
    protected String type;
    protected double balance;

    protected Account(double amount) {
        balance = amount;
    }

    public abstract void credit(double amount);

    public abstract void debit(double amount);

    public int getAccountNumber() {
        return accountNumber;
    }

    public String getType() {
        return type;
    }

    public double getBalance() {
        return balance;
    }

}
