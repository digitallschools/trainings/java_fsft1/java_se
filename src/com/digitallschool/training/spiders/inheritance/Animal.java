/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.inheritance;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Animal {

    public String name;
    public String type;
    public boolean alive;

    {
        alive = true;
    }

    public Animal() {
        super();
    }

    public void move(int distance) {
        System.out.println("Animal is moving: " + distance + " distance");
    }

    public void eat() {
        System.out.println("Animal is eating");
    }

    public static void whoAmI() {
        System.out.println("I am Animal");
    }
}
