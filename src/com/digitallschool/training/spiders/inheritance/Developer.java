/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.inheritance;

import com.digitallschool.training.spiders.exceptions.TaskOverflowException;
import java.util.Objects;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Developer extends Employee {

    private String technology;
   
    private final int DEVELOPER_TASK_LIMIT;
    private DeveloperTask[] tasks;
    
    {
        DEVELOPER_TASK_LIMIT = 4;
        tasks = new DeveloperTask[DEVELOPER_TASK_LIMIT];
    }

    public Developer(int employeeId, String name, String technology) {
        super(employeeId, name);
        setTechnology(technology);
    }
    
    public void assignTask(DeveloperTask task) throws TaskOverflowException {
        boolean assigned = false;

        for (int i = 0; i < tasks.length; i++) {
            if (Objects.isNull(tasks[i])) {
                tasks[i] = task;
                assigned = true;
                break;
            }
        }

        if (!assigned) {
            throw new TaskOverflowException("No scope for new Tasks");
        }
    }

    public int developerTaskSize() {
        int count = 0;
        for (DeveloperTask t : tasks) {
            if (Objects.nonNull(t)) {
                count++;
            } else {
                break;
            }
        }

        return count;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

}
