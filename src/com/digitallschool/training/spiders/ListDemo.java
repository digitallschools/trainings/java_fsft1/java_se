/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders;

import com.digitallschool.training.spiders.collections.EmployeeIdComparator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import com.digitallschool.training.spiders.inheritance.Employee;
import java.util.ArrayList;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class ListDemo {
    
    public static void main(String[] args) {
        List t = new ArrayList();
        
        t.add(new Employee(10, "James"));
        t.add(new Employee(8, "Yashwanth"));
        t.add(new Employee(2, "Balagurusamy"));
        
        System.out.println(t);
        
        t.sort(new EmployeeIdComparator());
        System.out.println(t);
    }
    
    public static void main1(String[] args) {
        List t = new LinkedList();
        
        t.add(4);
        t.add(2);
        t.add(6);
        t.add(8);
        t.add(5);
        
        System.out.println(t);

        //t.remove(new Integer(2));
        System.out.println(t);
        
        t.add(2, 5);
        System.out.println(t);
        
        t.set(2, 50);
        System.out.println(t);
        
        System.out.println(t.get(3));
        
        System.out.println("############");
        
        ListIterator it = t.listIterator(3);
        while (it.hasNext()) {
            System.out.println(it.next());
        }
        System.out.println("++++");
        while (it.hasPrevious()) {
            System.out.println(it.previous());
        }
        System.out.println("++++");
        while (it.hasNext()) {
            System.out.println(it.next());
        }
        
        System.out.println(t.indexOf(32));
    }
    
}
