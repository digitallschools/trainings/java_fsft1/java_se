/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.collections;

import com.digitallschool.training.spiders.inheritance.Employee;
import java.util.Comparator;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class EmployeeIdComparator implements Comparator{
    @Override
    public int compare(Object a, Object b){
        Employee x = (Employee)a;
        Employee y = (Employee)b;
        
        return x.getEmployeeId() - y.getEmployeeId();
    }
}
