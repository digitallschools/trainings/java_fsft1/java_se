/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.collections;

import com.digitallschool.training.spiders.inheritance.Employee;
import java.util.Comparator;
import java.util.Objects;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class EmployeeComparator implements Comparator<Employee>{
    @Override
    public int compare(Employee x, Employee y){
        if(Objects.equals(x.getEmployeeId(), y.getEmployeeId()) &&
                Objects.equals(x.getName(), y.getName())){
            return 0;
        }else if(!Objects.equals(x.getName(), y.getName())){
            return x.getName().compareTo(y.getName());
        }else{
            return x.getEmployeeId() - y.getEmployeeId();
        }
    }
}
