/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.basics;

import java.util.Arrays;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class SortingAlgorithms {

    public static void main(String[] args) {
        int[] input = {9, 7, 3, 5, 4};

        int currentIndex = 3;
        int currentValue = input[currentIndex];

        /*for (int i = currentIndex - 1; i >= 0; i--) {
            if (input[i] > currentValue) {
                input[i + 1] = input[i];
            } else {
                input[i + 1] = currentValue;
                break;
            }
        }*/
 /*for (int i = currentIndex; i >= 0; i--) {
            if (i == 0) {
                input[0] = currentValue;
                break;
            } else if (input[i - 1] > currentValue) {
                input[i] = input[i - 1];
            } else {
                input[i] = currentValue;
                break;
            }
        }*/
        for (int j = 1; j < input.length; j++) {
            int i = 0;
            currentValue = input[j];
            for (i = j - 1; i >= 0; i--) {
                if (input[i] > currentValue) {
                    input[i + 1] = input[i];
                } else {
                    break;
                }
            }

            input[i + 1] = currentValue;
        }

        System.out.println(Arrays.toString(input));
    }

    public static void main1(String[] args) {
        int[] input = {8, 1, 2, 6, 9, 3, 5, 4, 10, 7};

        System.out.println(Arrays.toString(input));

        //sort(input);
        selectionSort(input);
        System.out.println(Arrays.toString(input));
    }

    public static void sort(int[] values) {
        int min;
        int minIndex;

        //System.out.println("#############################");
        for (int i = 0; i < values.length - 1; i++) {
            /*min = min(values, i);
            minIndex = indexOf(values, min);*/

            minIndex = indexOfMin(values, i);
            swap(values, i, minIndex);

            //System.out.println(Arrays.toString(values));
        }
        //System.out.println("#############################");
    }

    public static int indexOfMin(int[] values, int fromIndex) {
        int minIndex = fromIndex;

        for (int i = fromIndex + 1; i < values.length; i++) {
            if (values[minIndex] > values[i]) {
                minIndex = i;
            }
        }

        return minIndex;
    }

    public static void swap(int[] values, int sourceIndex, int targetIndex) {
        int temp = values[sourceIndex];
        values[sourceIndex] = values[targetIndex];
        values[targetIndex] = temp;
    }

    public static int indexOf(int[] values, int value) {
        for (int i = 0; i < values.length; i++) {
            if (value == values[i]) {
                return i;
            }
        }

        return -1;
    }

    public static int min(int[] values, int fromIndex) {
        int min = values[fromIndex];

        for (int i = fromIndex + 1; i < values.length; i++) {
            if (min > values[i]) {
                min = values[i];
            }
        }

        return min;
    }

    public static void selectionSort(int[] values) {
        //System.out.println("################################");
        int temp;
        int minIndex;

        for (int i = 0; i < values.length - 1; i++) {
            minIndex = i;
            for (int j = i + 1; j < values.length; j++) {
                if (values[minIndex] > values[j]) {
                    minIndex = j;
                }
            }

            temp = values[i];
            values[i] = values[minIndex];
            values[minIndex] = temp;

            //System.out.println(Arrays.toString(values));
        }

        /*for(int i=0; i<values.length; i++){
            for(int j=i+1; j<values.length; j++){
                if(values[i] > values[j]){
                    temp = values[i];
                    values[i] = values[j];
                    values[j] = temp;
                }
            }
            System.out.println(Arrays.toString(values));
        }*/
 /*for(int i=0; i<values.length; i++){
            for(int j=0; j<values.length; j++){
                if(values[i] < values[j]){
                    temp = values[i];
                    values[i] = values[j];
                    values[j] = temp;
                }
            }
            System.out.println(Arrays.toString(values));
        }*/
        //System.out.println("################################");
    }
}
