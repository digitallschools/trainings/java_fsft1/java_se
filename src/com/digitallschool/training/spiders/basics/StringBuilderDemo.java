/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.basics;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class StringBuilderDemo {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        
        System.out.println(sb.capacity());
        
        for(int i=0; i<1000; i++){
            sb.append(i);
            //System.out.println(sb);
            System.out.print(sb.capacity() + " ");
        }
        
        System.out.println("#########################");
        
        for(int i=sb.length()-1; i>0; i--){
            sb.deleteCharAt(i);
            //System.out.println(sb);
            System.out.print(sb.capacity() + " ");
        }
        
        System.out.println("######################");
        sb.trimToSize();
        System.out.println(sb.capacity());
        
    }
    
    public static void main1(String[] args) {
        StringBuilder sb = new StringBuilder();
        
        System.out.println(sb.capacity());
        System.out.println(sb.length());
        
        sb.append("James");        
        System.out.println(sb);
        
        sb.append(4567);
        System.out.println(sb);
        
        String temp = sb.toString();
        System.out.println(temp);
        
        sb.insert(5, " Gosling. ");
        System.out.println(sb);
        
        sb.delete(15, 17);
        System.out.println(sb);
        
        sb.replace(15, 17, ""+89);
        System.out.println(sb);
        
        sb.reverse();
        System.out.println(sb);
        
        sb.reverse();
        sb.replace(15, 17, "" + 1234);
        System.out.println(sb);
        
        System.out.println(sb.codePointAt(0));
        
        System.out.println(sb.length());
        
        sb.setLength(sb.length()+5);
        
        sb.setCharAt(23, 'W');
        System.out.println(sb.length());
        
        sb.append("Hello");
        System.out.println(sb);
        System.out.println(sb.length());
        
        sb.setLength(12);
        System.out.println(sb.length());
        sb.append('k');
        System.out.println(sb);
        System.out.println(sb.length());
    }
}







