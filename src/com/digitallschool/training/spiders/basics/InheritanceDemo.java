/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.basics;

import com.digitallschool.training.spiders.inheritance.Account;
import com.digitallschool.training.spiders.inheritance.SavingAccount;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class InheritanceDemo {

    public static void main(String[] args) {
        Account a1 = new SavingAccount();
        System.out.println(a1.getBalance());

        Account a2 = new SavingAccount(5000);
        System.out.println(a2.getBalance());
    }

    /*public static void main5(String[] args) {
        Animal a1 = new Animal();
        Animal a2 = new Bird();
        
        Bird b1 = new Bird();
        
        a1.whoAmI();
        a2.whoAmI();
        
        a1.move(100);
        a2.move(100);
    }
    
    public static void main4(String[] args) {
        Animal a1 = new Animal();
        Animal a2 = new Animal();

        Animal a3 = new Bird();
        Animal a4 = new Bird();

        Bird b1 = new Bird();
        Bird b2 = new Bird();
        Parrot p1 = new Parrot();
        Parrot p2 = new Parrot();

        Snake s1 = new Snake();
        Snake s2 = new Snake();

        /*AnimalUtilities.compete(a1, a2);
        AnimalUtilities.compete(a3, a4);
        AnimalUtilities.compete(a1, b2);
        AnimalUtilities.compete(b1, a2);
        AnimalUtilities.compete(b1, b2);
        AnimalUtilities.compete(s1, s2);
        AnimalUtilities.compete(a1, s2);*/

 /*AnimalUtilities au = new AnimalUtilities();
        
        au.compete(a1, a2);
        System.out.println("");
        au.compete(a3, a4);
        System.out.println("");
        au.compete(a1, b2);
        System.out.println("");
        au.compete(b1, a2);
        System.out.println("");
        au.compete(b1, b2);
        System.out.println("");
        au.compete(s1, s2);
        System.out.println("");
        au.compete(a1, s2);
        System.out.println("");
        
        au.competeAgain(a1, b2);
        au.competeAgain(b1, a2);
        //au.competeAgain(b1, b2);
        
        au.max(10.0, 20);
        au.max(10, 20.0);
        //au.max(10, 20);
    }

    public static void main3(String[] args) {
        Snake s1 = new Snake();

        System.out.println(s1.name);
        System.out.println(s1.type);

        s1.eat();
        s1.move(80);
    }

    public static void main2(String[] args) {
        Bird b1 = new Bird();

        System.out.println(b1.name);
        System.out.println(b1.type);

        b1.eat();
        b1.move(80);
        b1.move(80);

    }

    public static void main1(String[] args) {
        Animal a1 = new Animal();

        System.out.println(a1.name);
        System.out.println(a1.type);

        a1.eat();
        a1.move(100);
    }*/
}
