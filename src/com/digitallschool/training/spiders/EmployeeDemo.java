/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class EmployeeDemo {

    public static void main(String[] args) {
        Employee.whoAreYou();

        Employee e1 = new Employee(10, "Kumar", "Manager");
        
        System.out.println(e1.toString());
    }
}
