/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.streams;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class CharacterStreamsDemo {
    public static void main(String[] args) {
        FileReader in = null;
        FileWriter out = null;

        try {
            in = new FileReader("C:\\Users\\rkvod\\Desktop\\sample\\streams.txt");
            out = new FileWriter("C:\\Users\\rkvod\\Desktop\\sample\\streams_2.txt");

            int v;

            while ((v = in.read()) != -1) {
                System.out.print((char) v);
                out.write(v);
            }

            System.out.println("File copied successfully");
        } catch (FileNotFoundException fnfe) {

        } catch (IOException ioe) {

        } finally {
            if (Objects.nonNull(in)) {
                try {
                    in.close();
                } catch (IOException ioe) {

                }
            }

            if (Objects.nonNull(out)) {
                try {
                    out.close();
                } catch (IOException ioe) {

                }
            }
        }

        System.out.println("");
    }
}






