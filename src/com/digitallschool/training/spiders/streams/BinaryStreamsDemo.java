/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.streams;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class BinaryStreamsDemo {

    public static void main(String[] args) {

        try (
                FileInputStream in = new FileInputStream("C:\\Users\\rkvod\\Desktop\\sample\\HowInTheHell.jpg");
                FileOutputStream out = new FileOutputStream("C:\\Users\\rkvod\\Desktop\\sample\\HowInTheHell_3.jpg");) {

            int v;

            LocalDateTime start = LocalDateTime.now();
            System.out.println(start.toString());

            while ((v = in.read()) != -1) {
                out.write(v);
            }

            System.out.println(LocalDateTime.now().toString());
            System.out.println(ChronoUnit.MILLIS.between(start,
                    LocalDateTime.now()));

            /*int counter = 0;

            byte[] data = new byte[100];
            while (in.read(data) > 0) {
                out.write(data);

                counter++;
            }
            System.out.println("File copied successfully: " + counter);*/
        } catch (FileNotFoundException fnfe) {

        } catch (IOException ioe) {

        }

    }

    public static void main2(String[] args) {
        FileInputStream in = null;
        FileOutputStream out = null;

        try {
            in = new FileInputStream("C:\\Users\\rkvod\\Desktop\\sample\\HowInTheHell.jpg");
            out = new FileOutputStream("C:\\Users\\rkvod\\Desktop\\sample\\HowInTheHell_1.jpg");

            int v, counter = 0;

            /*while ((v = in.read()) != -1) {
                //System.out.print((char) v);
                out.write(v);
                counter++;
            }*/
            byte[] data = new byte[100];
            while (in.read(data) > 0) {
                out.write(data);

                counter++;
            }

            System.out.println("File copied successfully: " + counter);
        } catch (FileNotFoundException fnfe) {

        } catch (IOException ioe) {

        } finally {
            if (Objects.nonNull(in)) {
                try {
                    in.close();
                } catch (IOException ioe) {

                }
            }

            if (Objects.nonNull(out)) {
                try {
                    out.close();
                } catch (IOException ioe) {

                }
            }
        }

        System.out.println("");
    }

    public static void main1(String[] args) {
        FileInputStream in = null;

        try {
            in = new FileInputStream("C:\\Users\\rkvod\\Desktop\\sample\\streams.txt");

            int v;

            while ((v = in.read()) != -1) {
                System.out.print((char) v);
            }

            /*v = in.read();
            
            System.out.println(Integer.toBinaryString(v));
            System.out.println(v);*/
        } catch (FileNotFoundException fnfe) {

        } catch (IOException ioe) {

        } finally {
            if (Objects.nonNull(in)) {
                try {
                    in.close();
                } catch (IOException ioe) {

                }
            }
        }

        System.out.println("");
    }
}
