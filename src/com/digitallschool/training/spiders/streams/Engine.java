/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.streams;

import java.io.Serializable;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Engine implements Serializable{

    private String model;
    private String brand;
    private String type;
    private double hp;
    private double price;

    public Engine() {
        setBrand("TATA");
        setModel("TDSi");
        setType("Diesel");
        setHp(1299);
        setPrice(115000);
    }

    public Engine(String brand, String model, String type, double hp, double price) {
        setBrand(brand);
        setModel(model);
        setType(type);
        setHp(hp);
        setPrice(price);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getHp() {
        return hp;
    }

    public void setHp(double hp) {
        this.hp = hp;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
