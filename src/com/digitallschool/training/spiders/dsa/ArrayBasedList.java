/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.dsa;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class ArrayBasedList<E> implements List<E> {

    private Object[] listData;
    private int size;

    {
        listData = new Object[16];
        size = 0;
    }

    public ArrayBasedList() {
        super();
    }

    public E set(int index, E e) {
        throw new UnsupportedOperationException();
    }

    public ArrayBasedList subList(int fromIndex, int toIndex) {
        ArrayBasedList temp = new ArrayBasedList();

        for (int i = fromIndex; i < toIndex; i++) {
            temp.add(get(i));
        }

        return temp;
    }

    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException();
    }

    public ListIterator<E> listIterator(int fromIndex) {
        throw new UnsupportedOperationException();
    }

    public int lastIndexOf(Object t) {
        int index = -1;

        for (int i = size() - 1; i >= 0; i--) {
            if (get(i).equals(t)) {
                index = i;
                break;
            }
        }

        return index;
    }

    public int indexOf(Object t) {
        for (int i = 0; i < size(); i++) {
            if (get(i).equals(t)) {
                return i;
            }
        }

        return -1;
    }

    public <T> T[] toArray(T[] v) {
        return (T[]) Arrays.copyOf(listData, size, v.getClass());
    }

    public Object[] toArray() {
        Object[] temp = new Object[size];

        for (int i = 0; i < size; i++) {
            temp[i] = listData[i];
        }

        return temp;
    }

    private boolean isValidIndex(int index) {
        if (index < 0 || index >= size) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");

        if (size > 0) {
            builder.append(listData[0]);

            for (int i = 1; i < size; i++) {
                builder.append(", " + listData[i]);
            }
        }

        builder.append("]");

        return builder.toString();
    }

    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    public boolean contains(Object e) {
        for (int i = 0; i < this.size(); i++) {
            if (listData[i].equals(e)) {
                return true;
            }
        }

        return false;
    }

    public boolean addAll(int index, Collection<? extends E> c) {
        /*for (int i = 0; i < c.size(); i++) {
            this.add(i + index, c.get(i));
        }*/

        int i = 0;
        Iterator it = c.iterator();
        while (it.hasNext()) {
            this.add(i + index, (E) it.next());
            i++;
        }

        return true;
    }

    public boolean addAll(Collection<? extends E> c) {
        boolean changed = false;

        /*for (int i = 0; i < c.size(); i++) {
            this.add(c.get(i));
            changed = true;
        }*/
        Iterator it = c.iterator();
        while (it.hasNext()) {
            this.add((E) it.next());
            changed = true;
        }

        return changed;
    }

    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    public boolean remove(Object e) {
        for (int i = 0; i < size; i++) {
            if (listData[i].equals(e)) {
                while (i < size - 1) {
                    listData[i] = listData[i + 1];
                    i++;
                }
                listData[size - 1] = null;
                size--;
                return true;
            }
        }

        return false;
    }

    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    public E remove(int index) throws IndexOutOfBoundsException {
        if (!isValidIndex(index)) {
            throw new IndexOutOfBoundsException();
        }

        Object temp = listData[index];
        for (int i = index; i < size - 1; i++) {
            listData[i] = listData[i + 1];
        }
        listData[size - 1] = null;
        size--;

        return (E) temp;
    }

    public void add(int index, E e) throws IndexOutOfBoundsException {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }
        if (Objects.equals(listData.length, size)) {
            increaseCapacity();
        }

        int position = size;
        //for (; position > index; position--) {
        while (position > index) {
            listData[position] = listData[position - 1];
            position--;
        }

        listData[index] = e;
        size++;
    }

    public E get(int index) throws IndexOutOfBoundsException {
        //if (index < 0 || index >= size) {
        if (!isValidIndex(index)) {
            throw new IndexOutOfBoundsException();
        }

        return (E) listData[index];
    }

    private void increaseCapacity() {
        Object[] temp = new Object[listData.length * 2];
        System.arraycopy(listData, 0, temp, 0, listData.length);
        int tempSize = size;
        clear();
        listData = temp;
        size = tempSize;
    }

    public boolean add(E current) {
        if (Objects.equals(listData.length, size)) {
            increaseCapacity();
        }

        listData[size] = current;
        size++;
        return true;
    }

    public void clear() {
        while (size > 0) {
            listData[size - 1] = null;
            size--;
        }
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        if (Objects.equals(size, 0)) {
            return true;
        }
        return false;
    }

    public Iterator<E> iterator() {
        return new PersonalIterator();
    }

    private class PersonalIterator<E> implements Iterator {

        private int cursor = -1;

        public boolean hasNext() {
            if (cursor < size() - 1) {
                return true;
            }
            return false;
        }

        public E next() {
            return (E) listData[++cursor];
        }
    }
}
