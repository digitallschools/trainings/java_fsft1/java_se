/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.dsa;

import com.digitallschool.training.spiders.inheritance.Developer;
import com.digitallschool.training.spiders.inheritance.Employee;
import com.digitallschool.training.spiders.menudrivenproject.Customer;
import java.util.Iterator;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class ArrayBasedListDemo {

    public static void main(String[] args) {
        ArrayBasedList<Developer> a = new ArrayBasedList<>();
        
        a.add(new Developer(123, "Bosch", "Java Collections"));
        
        Object[] t1 = a.toArray();
        Employee[] t2 = a.toArray(new Employee[0]);
        
        for(Employee e: t2){
            System.out.println(e);
        }
    }

    public static void main7(String[] args) {
        ArrayBasedList<String> a = new ArrayBasedList<>();
        ArrayBasedList<Customer> c = new ArrayBasedList<>();

        a.add("Hello");
        //a.add(45);
        //a.add(new Employee(10, "James"));

        String t1 = a.get(0);

        CharSequence[] t = a.toArray(new CharSequence[0]);

        for (CharSequence temp : t) {
            System.out.println(temp);
        }
    }

    public static void main6(String[] args) {
        ArrayBasedList a = new ArrayBasedList();

        a.add(5);
        a.add(10);
        a.add(15);

        System.out.println(a);
        Iterator it = a.iterator();
        Iterator it2 = a.iterator();
        /*while(it.hasNext()){
            System.out.println(it.next());
        }*/
        it2.next();
        for (int i = 0; i < 2; i++) {
            System.out.println(it.next() + " " + it2.next());
        }
    }

    public static void main5(String[] args) {
        ArrayBasedList a = new ArrayBasedList();
        ArrayBasedList b = new ArrayBasedList();

        a.add(5);
        a.add(10);
        a.add(15);

        b.add(18);
        b.add(19);

        System.out.println(a);
        System.out.println(b);

        a.addAll(1, b);
        System.out.println(a);
        System.out.println(b);
    }

    public static void main4(String[] args) {
        ArrayBasedList a1 = new ArrayBasedList();

        a1.add(25);
        a1.add(35);
        a1.add(45);
        a1.add(55);

        ArrayBasedList b1 = new ArrayBasedList();
        b1.add(25);
        b1.add(35);
        b1.add(45);

        System.out.println(a1);
        System.out.println(b1);

        for (int i = 0; i < b1.size(); i++) {
            a1.remove(b1.get(i));
            System.out.println(a1);
        }
    }

    public static void main3(String[] args) {
        ArrayBasedList a1 = new ArrayBasedList();

        a1.add(25);
        a1.add(35);
        a1.add(45);
        a1.add(55);

        System.out.println(a1);
        a1.remove(1);
        System.out.println(a1);
        a1.remove(new Integer(25));
        System.out.println(a1);
    }

    public static void main2(String[] args) {
        ArrayBasedList a1 = new ArrayBasedList();

        a1.add(25);
        a1.add(35);
        a1.add(45);
        a1.add(55);
        System.out.println(a1.size());
        System.out.println(a1.remove(1));
        System.out.println(a1.size());
        System.out.println(a1.get(a1.size() - 1));

        System.out.println();
    }

    public static void main1(String[] args) {
        ArrayBasedList a1 = new ArrayBasedList();

        System.out.println(a1.size());
        System.out.println(a1.isEmpty());

        a1.add(25);
        System.out.println(a1.size());
        System.out.println(a1.isEmpty());
    }
}
