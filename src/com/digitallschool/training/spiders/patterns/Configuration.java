/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.patterns;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public final class Configuration {

    private static Configuration instance;// = new Configuration();

    static {
        instance = new Configuration();

    }

    {
        try (BufferedReader in = new BufferedReader(new FileReader("C:\\Users\\rkvod\\spiders_menu_driven.conf"))) {
            customersSizePerScreen = Integer.parseInt(in.readLine());
            paymentsSizePerScreen = Integer.parseInt(in.readLine());
            purchasesSizePerScreen = Integer.parseInt(in.readLine());
        } catch (FileNotFoundException fnfe) {
            try (BufferedWriter out = new BufferedWriter(new FileWriter("C:\\Users\\rkvod\\spiders_menu_driven.conf"))) {
                out.write(10 + "\n");
                customersSizePerScreen = 10;

                out.write(10 + "\n");
                paymentsSizePerScreen = 10;

                out.write(10 + "\n");
                purchasesSizePerScreen = 10;

            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private Integer customersSizePerScreen;
    private Integer paymentsSizePerScreen;
    private Integer purchasesSizePerScreen;

    private Configuration() {
        super();
    }

    public static void saveConfig() {
        try (BufferedWriter out = new BufferedWriter(new FileWriter("C:\\Users\\rkvod\\spiders_menu_driven.conf"))) {
            out.write(instance.getCustomersSizePerScreen() + "\n");
            out.write(instance.getPaymentsSizePerScreen() + "\n");
            out.write(instance.getPurchasesSizePerScreen() + "\n");
        } catch (IOException ioe) {

        }
    }

    public static Configuration getInstance() {
        //if(instance == null){
        /*if(Objects.isNull(instance)){
            instance = new Configuration();
        }*/

        return instance;
    }

    public Integer getCustomersSizePerScreen() {
        return customersSizePerScreen;
    }

    public void setCustomersSizePerScreen(Integer customersSizePerScreen) {
        this.customersSizePerScreen = customersSizePerScreen;
    }

    public Integer getPaymentsSizePerScreen() {
        return paymentsSizePerScreen;
    }

    public void setPaymentsSizePerScreen(Integer paymentsSizePerScreen) {
        this.paymentsSizePerScreen = paymentsSizePerScreen;
    }

    public Integer getPurchasesSizePerScreen() {
        return purchasesSizePerScreen;
    }

    public void setPurchasesSizePerScreen(Integer purchasesSizePerScreen) {
        this.purchasesSizePerScreen = purchasesSizePerScreen;
    }

}
