/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders;

import java.time.LocalDateTime;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class StaticDemoSupport {
    public static void main(String[] args) throws Exception{
        System.out.println("START " + LocalDateTime.now());
        Thread.sleep(4000);        
        System.out.println(StaticDemo.one);        
        
        Thread.sleep(4000);
        System.out.println(StaticDemo.two);
        
        Thread.sleep(4000);
        System.out.println(StaticDemo.three);
    }
}
