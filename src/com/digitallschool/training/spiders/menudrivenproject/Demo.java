/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.menudrivenproject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Demo {

    public static void main(String[] args) {
        Map<Integer, Customer> customers = new TreeMap();

        try (DataInputStream in = new DataInputStream(
                new FileInputStream("C:\\Users\\rkvod\\Desktop\\sample\\customers_obj.dat"))) {

            while (true) {
                Integer customerId = in.readInt();

                customers.put(customerId,
                        new Customer(
                                customerId,
                                in.readUTF(),
                                in.readUTF(),
                                in.readUTF(),
                                LocalDate.parse(in.readUTF()),
                                in.readDouble()
                        )
                );
            }

        } catch (EOFException eofe) {
            System.out.println("Customer Data Loaded Successfully");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        for (Map.Entry<Integer, Customer> entry : customers.entrySet()) {
            System.out.println(entry.getValue());
        }

        customers.get(9).setPendingAmount(3500.0);

        for (Map.Entry<Integer, Customer> entry : customers.entrySet()) {
            System.out.println(entry.getValue());
        }

        /*List<Customer> customerList = new ArrayList();

        try (DataInputStream in = new DataInputStream(
                new FileInputStream("C:\\Users\\rkvod\\Desktop\\sample\\customers_obj.dat"))) {

            while (true) {
                customerList.add(
                        new Customer(
                                in.readInt(),
                                in.readUTF(),
                                in.readUTF(),
                                in.readUTF(),
                                LocalDate.parse(in.readUTF()),
                                in.readDouble()
                        )
                );
            }

        } catch (EOFException eofe) {
            System.out.println("Customer Data Loaded Successfully");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        
        System.out.println(customerList.size());
        
        for(Customer c: customerList){
            System.out.println(c);
        }*/
 /*List<Customer> customerList = new ArrayList();

        try (BufferedReader in = new BufferedReader(new FileReader("C:\\Users\\rkvod\\Desktop\\sample\\customers.dat"))) {
            String temp = "";

            while ((temp = in.readLine()) != null) {
                String values[] = temp.split(",");

                customerList.add(
                        new Customer(
                                Integer.parseInt(values[0]),
                                values[1],
                                values[2],
                                values[3],
                                LocalDate.parse(values[4]),
                                Double.parseDouble(values[5])
                        )
                );
            }

            for (Customer t : customerList) {
                if (Objects.equals(t.getCustomerId(), 9)) {
                    t.setPendingAmount(3500.0);
                    break;
                }
            }*/
 /*customerList.get(Collections.binarySearch(
                    customerList,
                    new Customer(8, null, null, null, null, 0.0)))
                    .setPendingAmount(3500.0);

            for (Customer t : customerList) {
                System.out.println(t);
            }
        } catch (IOException ioe) {

        }*/
    }

    public static void main2(String[] args) {
        List<Customer> customerList = new ArrayList();

        customerList.add(new Customer(5, "Dietel", "8909123456", "U.S.A.", LocalDate.now(), 0.0));
        customerList.add(new Customer(8, "Kanitkar", "7845671234", "India", LocalDate.now(), 0.0));
        customerList.add(new Customer(9, "James Gosling", "3456123456", "Canada", LocalDate.now(), 0.0));
        customerList.add(new Customer(12, "Balagurusamy", "6789012345", "India", LocalDate.now(), 0.0));
        customerList.add(new Customer(15, "Dietel", "8909123667", "U.S.A.", LocalDate.now(), 0.0));

        try (DataOutputStream out = new DataOutputStream(
                new FileOutputStream("C:\\Users\\rkvod\\Desktop\\sample\\customers_obj.dat"))) {
            for (Customer c : customerList) {
                out.writeInt(c.getCustomerId());
                out.writeUTF(c.getName());
                out.writeUTF(c.getMobile());
                out.writeUTF(c.getAddress());
                out.writeUTF(c.getAddedOn().toString());
                out.writeDouble(c.getPendingAmount());
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        /*try (FileWriter out = new FileWriter("C:\\Users\\rkvod\\Desktop\\sample\\customers.dat")) {
            for (Customer c : customerList) {
                out.write(c.getCustomerId() + ",");
                out.write(c.getName() + ",");
                out.write(c.getMobile() + ",");
                out.write(c.getAddress() + ",");
                out.write(c.getAddedOn() + ",");
                out.write(c.getPendingAmount() + "\n");
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }*/
    }
}
