package com.digitallschool.training.spiders;

import com.digitallschool.training.spiders.exceptions.CarPriceException;
import com.digitallschool.training.spiders.streams.Engine;
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Car implements Serializable{

    private double price;
    String color;
    public final String BRAND;
    public final String model;
    double cc;
    private String type;
    private boolean ignition;
    private double speed;

    private Engine engine;

    public static String category = "MOTOR VEHICLES";

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double currentSpeed() {
        return speed;
    }

    public void applyBreak(double step) {
        step = Math.abs(step);
        if (speed >= step) {
            speed -= step;
        } else {
            speed = 0;
        }
    }

    public void stop() {
        speed = 0;
    }

    public void accelerate(double step) {
        step = Math.abs(step);
        if (isOn()) {
            speed += step;
        }
    }

    public boolean isOn() {
        return ignition;
    }

    public void ignitionOn() {
        ignition = true;
    }

    public void ignitionOff() {
        ignition = false;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double p) throws CarPriceException { //IllegalArgumentException {
        if (p >= 100000) {
            price = p;
        } else {
            //System.out.println("Invalid Car Price");
            //IllegalArgumentException iae = new IllegalArgumentException();
            //throw iae;

            //throw new IllegalArgumentException("For car price of: " + p);
            throw new CarPriceException();//"For price of: " + p);
        }
        //price = p;
    }

    {
        //brand = "Audi";
        price = 3500000;
        color = "White";
        //model = "A3";
        cc = 2499;
        type = "Diesel";
        ignition = false;
        speed = 0;
    }

    public Car() {
        this("Audi");
        //BRAND = "Audi";
        //model = "A3";
        /*price = 3500000;
        color = "White";
        model = "A3";
        cc = 2499;
        type = "Diesel";*/
    }

    public Car(String b) {
        this(b, "A3");
        //BRAND = b;
        //model = "A3";
        /*price = 3500000;
        color = "White";
        model = "A3";
        cc = 2499;
        type = "Diesel";*/
    }

    public Car(String b, String m) {
        this(b, m, 3500000);
        //BRAND = b;
        //model = m;
        /*price = 3500000;
        color = "White";
        cc = 2499;
        type = "Diesel";*/
    }

    public Car(String b, String m, double p) {
        //super();
        BRAND = b;
        model = m;
        price = p;
    }
    public Car(String b, String m, double p, Engine engine) {
        //super();
        BRAND = b;
        model = m;
        price = p;
        this.engine = engine;
    }
}
