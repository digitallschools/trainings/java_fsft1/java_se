package com.digitallschool.training.spiders.refactor1;

import java.time.YearMonth;
import java.util.Date;

public class Order {

    int customerId;
    String customerName;

    String customerAddress1;
    String customerAddress2;
    String customerAddress3;
    String currentAddress;

    String mobileNumber1;
    String mobileNumber2;
    String mobileNumber3;
    String currentMobileNumber;

    int orderId;
    Date orderedOn;
    double amount;

    int[] orderItemIds;
    double[] orderItemPrices;
    int[] orderItemQuantities;

    boolean primeOrGatiShippingService;
    int primeShippingId;
    String primeShippingStatus;
    int gatiShippingId;
    String gatiShippingStatus;

    boolean creditOrDebitCardPayemnt;
    long creditCardNumber;
    String nameOnCreditCard;
    YearMonth creditCardExpiryDate;
    short creditCardCVVNumber;

    long debitCardNumber;
    String nameOnDebitCard;
    YearMonth debitCardExpiryDate;
    short debitCardCVVNumber;

    public void createOrder() {
        System.out.println("Order is created");
    }

    public void createCustomer() {
        throw new UnsupportedOperationException("To be implemented");
    }

    public boolean processOrder() {
        throw new UnsupportedOperationException("To be implemented");
    }

    public boolean processCCPayment() {
        throw new UnsupportedOperationException("To be implemented");
    }

    public boolean processPrimeShipping() {
        throw new UnsupportedOperationException("To be implemented");
    }

    public boolean processDCPayment() {
        throw new UnsupportedOperationException("To be implemented");
    }

    public boolean processGatiShipping() {
        throw new UnsupportedOperationException("To be implemented");
    }
}
