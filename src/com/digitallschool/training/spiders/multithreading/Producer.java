/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.multithreading;

import java.util.Optional;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Producer extends Thread {

    private Warehouse pw;
    private Optional<Integer> count;

    public Producer(Warehouse pw) {
        this.pw = pw;
        count = Optional.empty();
    }
    public Producer(Warehouse pw, Integer count){
        this.pw = pw;
        this.count = Optional.of(count);
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();

        int temp;
        for (int i = 0; i < count.orElse(10); i++) {
            temp = (int) (10 + Math.random() * 90);

            pw.setGoods(temp);
            System.out.println(name + ": " + temp);

            try {
                Thread.sleep(2000);
            } catch (InterruptedException ie) {

            }
            /*try {
                pw.setGoods(temp);
                System.out.println(name + ": " + temp);
            } catch (IllegalStateException ise) {
                System.out.println(ise.getMessage());
            }*/
        }
    }
}
