/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.multithreading;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class PCDemo {

    public static void main(String[] args) {
        Warehouse warehouse = new Warehouse();

        Producer a1 = new Producer(warehouse, 4);
        Consumer b1 = new Consumer(warehouse, 6);
        Producer a2 = new Producer(warehouse);
        Consumer b2 = new Consumer(warehouse, 8);

        a1.setName("A1");
        b1.setName("B1");
        a2.setName("A2");
        b2.setName("B2");

        b1.start();
        a1.start();
        b2.start();
        a2.start();
    }
}
