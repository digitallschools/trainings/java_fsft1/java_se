/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.multithreading;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class FutureDemo {

    public static void mainN(String[] args) {
        System.out.println(Runtime.getRuntime().availableProcessors());
        System.out.println(ForkJoinPool.getCommonPoolParallelism());
        System.out.println(ForkJoinPool.commonPool().getParallelism());
        System.out.println(ForkJoinPool.commonPool().getActiveThreadCount());
        System.out.println(ForkJoinPool.commonPool().getPoolSize());
        System.out.println(ForkJoinPool.commonPool().getAsyncMode());

    }

    public static void main(String[] args) {

        Callable<Integer[]> work1 = new CallableWork();

        CompletableFuture.supplyAsync(() -> {
            Integer[] values = new Integer[10];
            System.out.println(Thread.currentThread().getName());
            for (int i = 0; i < 10; i++) {
                values[i] = (int) (10 + Math.random() * 90);
                System.out.println("Work1: " + values[i]);

                try {
                    Thread.sleep(600);
                } catch (InterruptedException ie) {

                }
            }
            return values;
        }).thenAccept(v -> {
            System.out.println(Thread.currentThread().getName());
            for (int i = 0; i < v.length; i++) {
                System.out.println(v[i]);
                try {
                    Thread.sleep(400);
                } catch (InterruptedException ie) {

                }
            }
        });

        for (int i = 0; i < 3; i++) {
            System.out.println("Main: " + System.currentTimeMillis());
            try {
                Thread.sleep(1200);
            } catch (InterruptedException ie) {

            }
        }

        //ForkJoinPool.commonPool().awaitQuiescence(2000, TimeUnit.MILLISECONDS);
        System.out.println("Main Finished");
    }

    public static void main2(String[] args) {

        CountDownLatch latch = new CountDownLatch(1);
        Callable<Integer[]> work1 = new CallableWork();

        CompletableFuture.supplyAsync(() -> {
            Integer[] values = new Integer[10];
            System.out.println(Thread.currentThread().getName());
            for (int i = 0; i < 10; i++) {
                values[i] = (int) (10 + Math.random() * 90);
                System.out.println("Work1: " + values[i]);

                try {
                    Thread.sleep(400);
                } catch (InterruptedException ie) {

                }
            }
            //latch.countDown();
            return values;
        }).thenAcceptAsync(v -> {
            System.out.println(Thread.currentThread().getName());
            for (int i = 0; i < v.length; i++) {
                System.out.println(v[i]);
                try {
                    Thread.sleep(400);
                } catch (InterruptedException ie) {

                }
            }
            latch.countDown();
        });

        for (int i = 0; i < 3; i++) {
            System.out.println("Main: " + System.currentTimeMillis());
            try {
                Thread.sleep(1200);
            } catch (InterruptedException ie) {

            }
        }

        //ForkJoinPool.commonPool().awaitQuiescence(2000, TimeUnit.MILLISECONDS);
        System.out.println("Main Finished");

        try {
            latch.await();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        System.out.println("Main End");
    }

    public static void main1(String[] args) {
        ExecutorService es = Executors.newFixedThreadPool(2);

        Callable<Integer[]> work1 = new Callable<Integer[]>() {
            @Override
            public Integer[] call() {
                Integer[] values = new Integer[10];

                for (int i = 0; i < 10; i++) {
                    values[i] = (int) (10 + Math.random() * 90);
                    System.out.println("Work1: " + values[i]);

                    try {
                        Thread.sleep(800);
                    } catch (InterruptedException ie) {

                    }
                }
                return values;
            }
        };

        Runnable work2 = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    System.out.println("Worker: " + System.currentTimeMillis());
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ie) {

                    }
                }
            }
        };

        Future<Integer[]> work1F = es.submit(work1);
        Future work2F = es.submit(work2);

        for (int i = 0; i < 3; i++) {
            System.out.println("Main: " + System.currentTimeMillis());
            try {
                Thread.sleep(700);
            } catch (InterruptedException ie) {

            }
        }

        /*try {
            work2F.get();
        } catch (Exception ie) {

        }*/
        try {
            Integer[] v = work1F.get();
            for (int i = 0; i < v.length; i++) {
                System.out.println(v[i]);
            }
        } catch (Exception ie) {

        }

        System.out.println("Main finished");
        es.shutdown();
    }

    private static class RunnableWork implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                System.out.println("Worker: " + System.currentTimeMillis());
                try {
                    Thread.sleep(400);
                } catch (InterruptedException ie) {

                }
            }
        }
    }

    private static class CallableWork implements Callable {

        @Override
        public Integer[] call() {
            Integer[] values = new Integer[10];

            for (int i = 0; i < 10; i++) {
                values[i] = (int) (10 + Math.random() * 90);
                System.out.println("Work1: " + values[i]);

                try {
                    Thread.sleep(400);
                } catch (InterruptedException ie) {

                }
            }
            return values;
        }
    }
}
