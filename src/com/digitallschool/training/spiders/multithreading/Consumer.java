/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.multithreading;

import java.util.Optional;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Consumer extends Thread {

    private Warehouse cw;
    private Optional<Integer> count;

    public Consumer(Warehouse cw) {
        this.cw = cw;
        this.count = Optional.empty();
    }
    public Consumer(Warehouse cw, Integer count){
        this.cw = cw;
        this.count = Optional.of(count);
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();

        for (int i = 0; i < count.orElse(10); i++) {
            System.out.println(name + ": " + cw.getGoods());

            try {
                Thread.sleep(200);
            } catch (InterruptedException ie) {

            }
        }
    }
}
