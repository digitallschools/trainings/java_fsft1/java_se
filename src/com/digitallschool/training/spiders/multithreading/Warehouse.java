/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.multithreading;

import java.util.Objects;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Warehouse {

    private Integer goods;

    public Warehouse() {
        super();
    }

    public synchronized Integer getGoods() {
        /*while (true) {
            if (isEmpty()) {
                try {
                    wait();
                } catch (InterruptedException ie) {

                }
            } else {
                Integer temp = goods;
                goods = null;
                notifyAll();
                return temp;
            }
        }*/

        while (isEmpty()) {
            try {
                wait();
            } catch (InterruptedException ie) {

            }
        }

        Integer temp = goods;
        goods = null;
        notifyAll();
        return temp;
    }

    public synchronized void setGoods(Integer goods) {
        while (true) {
            if (isEmpty()) {
                this.goods = goods;
                notifyAll();
                return;
            } else {
                try {
                    this.wait();
                } catch (InterruptedException ie) {
                }
            }
        }
        /*if(!isEmpty()){
            throw new IllegalStateException("Warehouse not empty: " + goods + " discarded");
        }*/
        //this.goods = goods;
    }

    public boolean isEmpty() {
        if (Objects.nonNull(goods)) {
            return false;
        }
        return true;
    }
}
