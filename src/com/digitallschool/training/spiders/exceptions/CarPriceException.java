/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.exceptions;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class CarPriceException extends IllegalArgumentException {

    public CarPriceException() {
        super();
    }

    public CarPriceException(String message) {
        super(message);
    }

    public CarPriceException(Throwable problem) {
        super(problem);
    }

    public CarPriceException(String message, Throwable problem) {
        super(message, problem);
    }
}
