/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.exceptions;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class TaskOverflowException extends RuntimeException {

    public TaskOverflowException(String message) {
        super(message);
    }
}
