/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class SortingAlgorithms {

    public static void main(String[] args) {
        int a = 10, b = 20;
        
        System.out.println(a + " " + b);
        
        swap(a, b);
        System.out.println(a + " " + b);
    }
    
    public static void swap(int x, int y){
        int temp = x;
        x = y;
        y = temp;
    }
    
    public static void main1(String[] args) {
        //int[] a = {18, 25, 14, 6, 32, 21, 13, 15};
        int[] a = {6, 13, 18, 14, 15, 32, 21, 25};

        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");

        bubbleSort(a);
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
    }

    public static void bubbleSort(int[] x) {
        int temp, count = 0;
        boolean swap = false;

        for (int i = 0; i < x.length - 1; i++) {
            swap = false;

            for (int j = 0; j < x.length - 1 - i; j++) {
                count++;
                if (x[j] > x[j + 1]) {
                    temp = x[j];
                    x[j] = x[j + 1];
                    x[j + 1] = temp;
                    swap = true;
                }
            }

            if (!swap) {
                break;
            }
        }
        System.out.println(count);
    }
}
