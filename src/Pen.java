/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class Pen {
    final String BRAND;
    double price;
    
    {
        price = 15.0;
    }
    
    public Pen(){
        BRAND = "Reynolds";
    }
    
    public void write(String s){
        System.out.println(s);
    }
    
    public void write(int i){
        System.out.println(i);
    }
    
    public void write(double d){
        System.out.println(d);
    }
    
    public void write(boolean b){
        System.out.println(b);
    }
    
    public void write(char c){
        System.out.println(c);
    }
    
}







