/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class EnhancedFor {

    public static void main(String[] args) {
        int[][] values
                = {
                    {10, 12, 18},
                    {22, 24, 26, 28},
                    {36, 39}
                };

        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < values[i].length; j++) {
                System.out.print(values[i][j] + " ");
            }
            System.out.println("");
        }
        System.out.println("");
        
        for(int[] t : values){
            for(int x : t){
                System.out.print(x + " ");
            }
            System.out.println("");
        }
    }

    public static void main1(String[] args) {
        int[] values = {10, 15, 20, 30, 50, 85, 90};

        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i]);
        }

        System.out.println("");
        for (int t : values) {
            System.out.println(t);
        }
    }
}
