
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class RandomHero {

    public static void main(String[] args) {
        List<String> heros = Arrays.asList(
                //"Vignesh",
                "Jasper",
                "Sharmati",
                "Parameshwari",
                //"Pavithra",
                "Hari",
                "Satya",
                "Ramizuddin",
                "Vijay",
                "Aishwarya",
                //"Komali",
                "Rajendra"
        );

        for (int i = 0; i < 12; i++) {
            Collections.shuffle(heros);

            try {
                Thread.sleep(1500);
                System.out.println("Shuffle: " + (i + 1));
            } catch (InterruptedException ie) {

            }
        }

        System.out.println(heros.get(0));
    }
}
