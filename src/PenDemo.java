/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class PenDemo {
    public static void main(String[] args) {
        Pen p1 = new Pen();
        
        System.out.println(p1.BRAND);
        System.out.println(p1.price);
        
        p1.write("Hello");
        
        p1.write("James Gosling");
        
        p1.write(2019);
    }
}
