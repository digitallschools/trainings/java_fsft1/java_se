
import com.digitallschool.training.spiders.inheritance.Developer;
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class ArraysDemo {

    public static void main(String[] args) {
        int[] ia = {10, 20, 30};
        
        int[] ib = Arrays.copyOf(ia, 2);
        
        for(int a: ib){
            System.out.println(a);
        }
        
        ib[0] = 40;
        System.out.println(ib[0]);
    }
    
    public static void main4(String[] args) {
        int[] ia = {10, 20, 30};
        String[] sa = {"James", "Johnson", "Dennis"};
        Developer[] da = {
            new Developer(10, "James", "Java"),
            new Developer(15, "Johnson", "Spring"),
            new Developer(20, "Dennis", "Sea")
        };
    }

    public static void main3(String[] args) {
        int[] a = {310, 310, 275, 275, 260, 290, 260, 260, 230, 230, 300, 310};
        int profit = 0;

        for (int sd = 1; sd < a.length; sd++) {
            for (int bd = 0; bd < sd; bd++) {
                //profit = Math.max(a[sd] - a[bd], profit);

                if (a[sd] - a[bd] > profit) {
                    profit = a[sd] - a[bd];
                }
            }
        }

        System.out.println("Profit: " + profit);
    }

    public static void main2(String[] args) {
        int[] a = {5, 10, 15, 20, 28, 26, 37, 58};

        printArray(a);

        rotate(a, 3);
        printArray(a);
    }

    public static void rotate(int[] x, int size) {
        int temp;

        for (int i = 0; i < size; i++) {
            temp = x[x.length - 1];
            for (int j = x.length - 2; j >= 0; j--) {
                x[j + 1] = x[j];
            }
            x[0] = temp;
        }
    }

    public static void printArray(int[] x) {
        for (int i = 0; i < x.length; i++) {
            System.out.print(x[i] + " ");
        }
        System.out.println("");
    }

    public static void main1(String[] args) {
        int[][] a = {{5, 10, 15}, {20, 28}, {26, 37, 58}};

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
