/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class WaterBottleDemo {

    public static void main(String[] args) {
        WaterBottle a = new WaterBottle();
        
        System.out.println(a.capacity);
        //System.out.println(a.quantity);
        System.out.println(a.getQuantity());
        
        //a.quantity = 800;
        a.setQuantity(350);
        System.out.println(a.getQuantity());
    }
    
    public static void main2(String[] args) {
        WaterBottle a = new WaterBottle();
        //System.out.println(a.price);
        System.out.println(a.getPrice());
        
        //double v = a.getPrice();
        //System.out.println(v);
        
        //a.price = -5.0;
        //System.out.println(a.price);
        
        a.setPrice(-10);
        System.out.println(a.getPrice());
    }
    
    public static void main1(String[] args) {
        WaterBottle a = new WaterBottle();

        System.out.println(a.brand);
        System.out.println(a.color);
        //System.out.println(a.price);
        System.out.println(a.capacity);
        //System.out.println(a.quantity);

        //a.brand = "Aquafina";
        WaterBottle b = new WaterBottle("Kingfisher", 2000);
        System.out.println(b.brand);
        System.out.println(b.capacity);
        //System.out.println(b.price);

        WaterBottle c = new WaterBottle("Aquafina");
        System.out.println(c.brand);

        WaterBottle d = new WaterBottle(5000);
    }
}
