/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class WaterBottle {

    final String brand;
    String color;
    private double price;
    final int capacity;
    private int quantity;
    private boolean open;

    public void empty() {
        quantity = 0;
    }

    public void empty(int q) {
        if (q < 0) {
            System.out.println("Invalid empty quantity");
            return;
        }

        if (quantity - q >= 0) {
            quantity -= q;
        } else {
            System.out.println("Underflow problem");
        }
    }

    public void fill() {
        quantity = capacity;
    }

    public void fill(int q) {
        if (q < 0) {
            System.out.println("Invalid fill quantity");
            return;
        }

        if (q + quantity <= capacity) {
            quantity += q;
        } else {
            System.out.println("Overflow problem");
        }
    }

    public void open() {
        open = true;
    }

    public void close() {
        open = false;
    }

    public boolean isOpen() {
        return open;
    }

    public void setQuantity(int v) {
        if (v < 0 || v > capacity) {
            System.out.println("Overflow/Underflow problem");
        } else {
            quantity = v;
        }
    }

    public int getQuantity() {
        return quantity;
    }

    public void setPrice(double v) {
        if (v < 10) {
            System.out.println("Invalid price");
        } else {
            price = v;
        }
    }

    public double getPrice() {
        return price;
    }

    public WaterBottle() {
        brand = "Bisleri";
        color = "Blue";
        price = 15.0;
        capacity = 750;
        quantity = 750;
    }

    public WaterBottle(String b) {
        brand = b;
        color = "Blue";
        price = 15.0;
        capacity = 750;
        quantity = 750;
    }

    public WaterBottle(String b, int c) {
        brand = b;
        capacity = c;
        quantity = c;
        color = "Blue";
        price = capacity / 1000 * 15.0;
    }

    public WaterBottle(int c) {
        brand = "Bisleri";
        capacity = c;
        quantity = c;
        color = "Blue";
        price = capacity / 1000 * 15.0;
    }

}
