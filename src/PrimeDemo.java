/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class PrimeDemo {

    public static void main(String[] args) {
        System.out.println(isPrime(25));
        System.out.println(isPrime(23));
        System.out.println(isPrime(103));
        
        printPrimes(15);
    }
    
    static void printPrimes(int count){
        int current = 2;
        int found = 0;
        
        outer:
        while(found < count){
            while(true){
                if(isPrime(current)){
                    System.out.println(current);
                    found++;
                    current++;
                    continue outer;
                }
                
                current++;
            }
        }
    }

    static boolean isPrime(int x) {
        if (x <= 1) {
            return false;
        }

        for (int i = 2; i < x; i++) {
            if (x % i == 0) {
                return false;
            }
        }

        return true;
    }
}
