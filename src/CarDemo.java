
import com.digitallschool.training.spiders.Car;
import com.digitallschool.training.spiders.exceptions.CarPriceException;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class CarDemo {

    public static void main(String[] args) {
        Car c1 = new Car();

        System.out.println(c1.getPrice());
        c1.setPrice(125000);
        System.out.println(c1.getPrice());

        try {
            c1.setPrice(25000);
        } catch (CarPriceException iae) {
            iae.printStackTrace();
        }

        System.out.println(c1.getPrice());
    }

    public static void main12(String[] args) {
        Car c1 = new Car();

        System.out.println(c1.getPrice());
        c1.setPrice(125000);
        System.out.println(c1.getPrice());
        try {
            c1.setPrice(25000);
        } catch (IllegalArgumentException iae) {
            iae.printStackTrace();
        }
        System.out.println(c1.getPrice());
    }

    public static void main11(String[] args) {
        Car c1 = new Car();
        System.out.println(c1.getType());

        c1.setType("Electric");
        System.out.println(c1.getType());
    }

    public static void main10(String[] args) {
        Car c1 = new Car("Toyota", "Camri");
        Car c2 = new Car("Tata", "Safari");

        System.out.println(c1.BRAND);
        System.out.println(c2.BRAND);

        System.out.println(c1.category);
        System.out.println(c2.category);

        System.out.println(c1.getPrice());
        System.out.println(c2.getPrice());

        c1.setPrice(125000);
        System.out.println(c1.getPrice());
        System.out.println(c2.getPrice());

        c1.category = "AUTOMOBILES";
        System.out.println(c1.category);
        System.out.println(c2.category);

        Car c3 = new Car();
        System.out.println(c3.category);
    }

    public static void main9(String[] args) {
        Car c1 = new Car();
        c1.ignitionOn();

        System.out.println(c1.currentSpeed());

        c1.accelerate(-25);

        System.out.println(c1.currentSpeed());
    }

    public static void main8(String[] args) {
        Car c1 = new Car("Tata", "Safari", 1450000);
        System.out.println(c1.BRAND);
    }

    public static void main7(String[] args) {
        Car c1 = new Car("Tata", "Safari", 1450000);
        System.out.println(c1.BRAND);
        System.out.println(c1.model);
        //System.out.println(c1.price);
        System.out.println(c1.getPrice());

        //c1.price = -860000;
        c1.setPrice(-860000);
        System.out.println(c1.getPrice());
    }

    /*public static void main6(String[] args) {
        Car c1 = new Car("Tata", "Safari");
        System.out.println(c1.BRAND);
        
        //c1.BRAND = "Mahindra";
        System.out.println(c1.BRAND);
    }
    
    public static void main5(String[] args) {
        Car c1 = new Car("Tata", "Safari");
        System.out.println(c1.BRAND);
        System.out.println(c1.model);
        System.out.println(c1.price);
        System.out.println(c1.type);
    }
    
    public static void main4(String[] args) {
        Car c1 = new Car();
        System.out.println(c1.BRAND);
        System.out.println(c1.model);
        
        Car c2 = new Car("Tata");
        System.out.println(c2.BRAND);
        System.out.println(c2.model);
        
        Car c3 = new Car("Mahindra");
        System.out.println(c3.BRAND);
        System.out.println(c3.model);
    }
    
    public static void main3(String[] args) {
        Car c1 = new Car();
        System.out.println(c1.BRAND);
        System.out.println(c1.price);

    }

    public static void main2(String[] args) {
        Car c1 = new Car();

        System.out.println(c1.BRAND);

        //c1.BRAND = "Audi";
        System.out.println(c1.BRAND);

        //c1.BRAND = "Ford";
        System.out.println(c1.BRAND);
    }

    public static void main1(String[] args) {
        Car c1;

        c1 = new Car();

        System.out.println(c1.price);
        System.out.println(c1.BRAND);

        Car c2 = new Car();
        System.out.println(c2.price);
        System.out.println(c2.BRAND);

        c1.price = 785000;
        System.out.println(c1.price);
        System.out.println(c2.price);
    }*/
}
